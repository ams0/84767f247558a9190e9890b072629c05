#!/bin/bash

red=`tput setaf 1`
reset=`tput sgr0`

subscription=$(az account show -o tsv --query id)

echo "Checking subscription $subscription"

for account in `az storage account list -o tsv --query [].name`
do
  for container in `az storage container list --auth-mode login --account-name $account -o tsv --query [].name 2>/dev/null`
  do
    #echo "  ...checking container $container in storage account $account"
    public=$(az storage container show  --auth-mode login --account-name $account --name $container -o json --query properties.publicAccess)
    if [[ $public == *"container"* ]]
      then
        echo "container $container in storage account $account is ${red}insecure!${reset}"
      fi
  done
#echo "Done with analysis of account $account"
done